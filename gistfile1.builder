# This is a fragment of my debugger session

   10    def move direction
   11      if [:up, :down].member? direction
   12        transaction do
   13          brothers = self.person.person_projects
   14          brothers.each_with_index do |brother, index|
   15            if direction == :up   && index == self.position - 1 or 
   16               direction == :down && index == self.position + 1
   17              begin
   18                self.position, brother.position = brother.position, self.position
   19              rescue

(rdb:19) self.position
2

(rdb:19) brother 
#<PersonProject id: 1, person_id: 1, project_id: 10, role: "administrator", position: 3, created_at: "2010-06-11 00:23:23", updated_at: "2010-06-12 19:14:49">

(rdb:19) brother.position
NoMethodError Exception: undefined method `position' for #<PersonProject:0xb59f5028>

(rdb:19) brother.read_attribute :position
3

(rdb:19) PersonProject.find(1).position
3


# Summary: I can use object.read_attribute :position, but not object.position !